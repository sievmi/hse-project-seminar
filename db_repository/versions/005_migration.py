from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
post = Table('post', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('text', String(length=500)),
    Column('timestamp', DateTime),
    Column('imagePath', String(length=500)),
    Column('day', Boolean),
    Column('week', Boolean),
    Column('month', Boolean),
    Column('year', Boolean),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['post'].columns['day'].create()
    post_meta.tables['post'].columns['month'].create()
    post_meta.tables['post'].columns['week'].create()
    post_meta.tables['post'].columns['year'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['post'].columns['day'].drop()
    post_meta.tables['post'].columns['month'].drop()
    post_meta.tables['post'].columns['week'].drop()
    post_meta.tables['post'].columns['year'].drop()
