import os
basedir = os.path.abspath(os.path.dirname(__file__))

ALLOWED_EXTENSION = set(['jpeg', 'jpg'])
UPLOAD_IMAGES = 'app/static/notesImages'
IMAGES_PATH = 'notesImages'

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')

CSRF_ENABLED = True
SECRET_KEY = 'you-will-never-guess'