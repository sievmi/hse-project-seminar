from app import db

class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(500))
    timestamp = db.Column(db.DateTime)
    imagePath = db.Column(db.String(500))
    day = db.Column(db.Boolean)
    week = db.Column(db.Boolean)
    month = db.Column(db.Boolean)
    year = db.Column(db.Boolean)



class Habit(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    active = db.Column(db.Boolean)
    delta = db.Column(db.Integer)
    lastStop = db.Column(db.Date)
    results = db.relationship('ResultHabit', backref = 'name', lazy = 'dynamic')

class ResultHabit(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date)
    result = db.Column(db.Integer)
    habit_id = db.Column(db.Integer, db.ForeignKey('habit.id'))