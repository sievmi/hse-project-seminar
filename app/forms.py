from flask.ext.wtf import Form
from wtforms import TextField, SelectField, IntegerField
from flask_wtf.html5 import DateField
from wtforms.validators import Required,optional
from models import Habit
import datetime

class PostForm(Form) :
    text = TextField('Text_post', validators=[Required()])

class AddHabitForm(Form):
    name = TextField('Name_add', validators=[Required()])

    def checkName(self):
        habit = Habit.query.filter_by(name = self.name.data).all()
        if habit:
            return True
        else:
            return False

class DeleteHabitForm(Form):
    select =  SelectField('Habits_delete', coerce=int)

class ChooseHabit(Form):
    select =  SelectField('chooseHabit', coerce=int)

class ChooseDate(Form):
    date = DateField('Date', validators=[Required()])

class ResultHabitForm(Form):
    date = DateField('Result_date', validators=[Required()])
    result = SelectField('Result_addRes', choices=[('1', 'Done'), ('0', 'Not done') ])
    habitname = SelectField('HabitName_addRees', coerce=int)

class GetResultsHabitForm(Form):
    year = IntegerField('Year_get', validators=[Required()])
    month = SelectField('Month_get', choices=[('1', 'January'), ('2', 'February') , ('3', 'Marh'),
                                          ('4', 'April'), ('5', 'May'), ('6', 'June'), ('7', 'July'),
                                          ('8', 'August'),  ('9', 'September'),
                                          ('10', 'October'), ('11', 'November'), ('12', 'December') ])


    def validate(self):
        rv = Form.validate(self)
        if not rv:
            return False
        if 1980 <= int(self.year.data) <= 2100:
            return True
        else:
            return False




