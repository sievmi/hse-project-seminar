/**
 * Created by Евгений on 08.05.2016.
 */

$(document).ready(function(){
    $(".editPostButton").click(function(){
        var postText = $(this).siblings(".postText");
        $(this).hide();
        $(this).next().show();
        var text = postText.text();
        postText.before("<div>" +
                "<textarea rows='4' cols='80' class='form-control'></textarea>" +
                "</div>");
        var textArea = postText.prev().find("textarea");
        textArea.text(text.trim());
        postText.hide();

        var image = $(this).siblings("img");
        if(image.size() > 0) {
            $(this).siblings(".deletePostImageButton").show();
        } else {
            $(this).siblings(".uploadImageForm").show();
        }
    });

    $(".deletePostImageButton").click(function() {
        $(this).hide();
        $(this).siblings(".uploadImageForm").show();
        $(this).siblings("img").remove();

        $.post("/notes",
            {
                type: "deleteImage",
                text: $(this).siblings(".postText").text().trim(),
            },
            function(data, status){
            }
        );
    });

    $(".savePostButton").click(function(){
        var postText = $(this).siblings(".postText");
        var textArea = postText.prev().find("textarea");

        var oldText = postText.text().trim();
        var newText = textArea.val().trim();
        if(newText.length == 0) return;

        postText.text(newText);
        textArea.remove();
        postText.show();
        $(this).hide();
        $(this).siblings(".editPostButton").show();
        $(this).siblings(".deletePostImageButton").hide();
        $(this).siblings(".uploadImageForm").hide();

        var upInput = $(this).siblings(".uploadImageForm").children(".uploadPostImageInput")
        var img = null;
        var elem = upInput.get()[0];
        if('files' in elem && elem.files.length > 0) {
            var sent_data = new FormData;
            sent_data.enctype = "multipart/form-data";
            sent_data.append('img', elem.files[0]);
            sent_data.append('text', oldText);
            sent_data.append('typeQ', 'addImage');
            $.ajax({
                type: 'POST',
                url: '/notes',
                data: sent_data,
                processData: false,
                contentType: false
            });
            //alert(elem.files[0].name + " " + elem.files[0].size);

             var reader = new FileReader();
             reader.onload = function (e) {
                postText.after("<img src='"+ e.target.result+"' width='890px'>");
                //alert(e.target.result);
                //post_body.find('img').attr('src', e.target.result);
            };
            //post_body.find('.content').after(img_tag);
            var files = $(this).siblings(".uploadImageForm").children(".uploadPostImageInput").prop('files');
            $(this).siblings(".uploadImageForm").ready(function () {
                //alert(":(");
                reader.readAsDataURL(files[0]);
                //alert("OKOK");
            });
        }

        $.post("/notes",
            {
                type:"edit",
                oldText: oldText,
                newText: newText
            },
            function(data, status){
            }
        );
    });

    $(".deletePostButton").click(function(){
        $(this).parent().hide('slow');
        $.post("/notes",
            {
                type:"delete",
                text: $(this).siblings(".postText").text()
            },
            function(data, status){
            });
        });



    $(".hideShowBlock").click(function(){
        var x = $(this).next();
        $(this).next().toggle();
    });

    $(".chooseButtonDay").click(function(){
        $.post("/rankNotes",
            {
                type:"choosePostDay",
                text: $(this).siblings(".postText").text()
            },
            function(data, status){
            }
        );
    });

    $(".chooseButtonWeek").click(function(){
        $.post("/rankNotes",
            {
                type:"choosePostWeek",
                text: $(this).siblings(".postText").text()
            },
            function(data, status){
            }
        );
    });

    $(".chooseButtonMonth").click(function(){
        $.post("/rankNotes",
            {
                type:"choosePostMonth",
                text: $(this).siblings(".postText").text()
            },
            function(data, status){
            }
        );
    });

    $(".chooseButtonYear").click(function(){
        $.post("/rankNotes",
            {
                type:"choosePostYear",
                text: $(this).siblings(".postText").text()
            },
            function(data, status){
            }
        );
    });

    $("#addHabit").children("div").children("input[type='button']").click(function(){
        var ID = -1;
        var text = $("#addHabit").find("input[type='text']").val().trim();
        alert("OK");
        $.post("/habits", {
            type: "addHabit",
            name: text,
        }, function(data, status){
            ID = parseInt(data);

            if(ID != -1){
                var elem = document.createElement("option");
                elem.setAttribute("value", ID);
                elem.innerHTML = text   ;
                $(".selectHabit").append(elem);
            }
        }
        );
    });

    $("#deleteHabit").find("input[type='button']").click(function() {
        habitID = $("#deleteHabit").find("select").val();
        $.post("/habits", {
            type: "deleteHabit",
            id: habitID
        }, function(data, status){
            ID = parseInt(data);
            $(".selectHabit").children("[value="+ID+"]").remove();
            //alert(data + " " + status);
        });

    });
});
