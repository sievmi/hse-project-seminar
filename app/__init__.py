from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask_images import Images

app = Flask(__name__)
db = SQLAlchemy(app)
images = Images(app)
app.config.from_object('config')

from app import models
from app.views import views
