from flask import render_template
from app.models import Habit, ResultHabit
from app.forms import ChooseHabit
from app import db

def habitStats():
    habits = Habit.query.all()
    chooseForm = ChooseHabit()
    chooseForm.select.choices = [(habit.id, habit.name) for habit in habits]

    if chooseForm.validate_on_submit():
        habit = Habit.query.get(chooseForm.select.data)
        results = ResultHabit.query.filter_by(name=habit).all()
        if len(results) == 0:
            return render_template("habitsStats.html",
                               chooseForm=chooseForm, message=True)
        stats = {}
        stats['name'] = habit.name
        stats['results'] = results
        resDone = filter(lambda x:x.result==True, results)
        stats['resDone'] = resDone
        firstDay = min(h.date for h in resDone)
        lastDay = max(h.date for h in resDone)
        stats['firstDay'] = firstDay
        stats['lastDay'] = lastDay
        totalDays = (lastDay-firstDay).days+1
        stats['totalDays'] = totalDays
        doneDays = len(resDone)
        stats['doneDays'] = doneDays




        return render_template("habitsStats.html",
                               chooseForm=chooseForm, stats = stats)

    return render_template("habitsStats.html", chooseForm=chooseForm)
