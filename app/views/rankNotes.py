from app.forms import ChooseDate
from flask import render_template
from time import gmtime, strftime
from datetime import datetime,timedelta
from app.models import Post
from app import db

def getStartWeek(date):
    startWeek = date
    while(startWeek.weekday() != 0):
        startWeek -= timedelta(days=1)
    return startWeek

def getEndWeek(date):
    startWeek = getStartWeek(date)
    return startWeek + timedelta(days=6)

def getStartMonth(date):
    startMonth = date
    while(startMonth.month == (startMonth-timedelta(days=1)).month):
        startMonth -= timedelta(days=1)
    return startMonth

def getEndMonth(date):
    endMonth = date
    while(endMonth.month == (endMonth+timedelta(days=1)).month):
        endMonth += timedelta(days=1)
    return endMonth

def getStartYear(date):
    startYear = datetime(year=date.year, month=1, day = 1)
    return startYear.date()

def getEndYear(date):
    endYear = datetime(year=date.year, month=12, day = 31)
    return endYear.date()

def getPostsDay(date, allPosts):
    postsDay = filter(lambda x: x.timestamp.date()==date, allPosts)
    return postsDay

def getPostsWeek(startWeek, endWeek, allPosts):
    postsWeek = filter(lambda x: startWeek <= x.timestamp.date() <= endWeek and x.day == True, allPosts)
    return postsWeek

def getPostsMonth(startMonth, endMonth, allPosts):
    postsMonth = filter(lambda x: startMonth <= x.timestamp.date() <= endMonth and x.week==True, allPosts)
    return postsMonth

def getPostsYear(startYear, endYear, allPosts):
    postsYear = filter(lambda x: startYear <= x.timestamp.date() <= endYear and x.month==True,  allPosts)
    return postsYear

def rankNotes(request):
    chooseDate = ChooseDate()
    date = datetime.today().date()
    if chooseDate.validate_on_submit():
        date = chooseDate.date.data

    startWeek = getStartWeek(date)
    endWeek = getEndWeek(date)
    startMonth = getStartMonth(date)
    endMonth = getEndMonth(date)
    startYear = getStartYear(date)
    endYear = getEndYear(date)

    allPosts = Post.query.all()
    postsDay = getPostsDay(date, allPosts)
    postsWeek = getPostsWeek(startWeek, endWeek, allPosts)
    postsMonth = getPostsMonth(startMonth, endMonth, allPosts)
    postsYear = getPostsYear(startYear, endYear, allPosts)

    if request.method == 'POST':
        if('type' in request.form):
            if(request.form['type'].strip() == 'choosePostDay'):
                text = request.form['text'].strip()
                post = Post.query.filter_by(text=text).first()
                oldBest = filter(lambda x: x.timestamp.date() == post.timestamp.date() and
                   x.day == True, allPosts)
                if(oldBest):
                    oldBest[0].day = False
                post.day = True
                db.session.commit()
            if(request.form['type'].strip() == 'choosePostWeek'):
                text = request.form['text'].strip()
                post = Post.query.filter_by(text=text).first()
                oldBest = filter(lambda x: x.week == True, postsWeek)
                if(oldBest):
                    oldBest[0].week = False
                post.week = True
                db.session.commit()
            if(request.form['type'].strip() == 'choosePostMonth'):
                text = request.form['text'].strip()
                post = Post.query.filter_by(text=text).first()
                oldBest = filter(lambda x: x.month == True, postsMonth)
                if(oldBest):
                    oldBest[0].month = False
                post.month = True
                db.session.commit()
            if(request.form['type'].strip() == 'choosePostYear'):
                text = request.form['text'].strip()
                post = Post.query.filter_by(text=text).first()
                oldBest = filter(lambda x: x.year == True, postsYear)
                if(oldBest):
                    oldBest[0].year = False
                post.year = True
                db.session.commit()




    return render_template("rankNotes.html", chooseDate=chooseDate,
            day=date, startWeek=startWeek, endWeek=endWeek,
            startMonth=startMonth, endMonth=endMonth,
            startYear=startYear, endYear=endYear,
            postsDay=postsDay, postsWeek=postsWeek,
            postsMonth=postsMonth, postsYear=postsYear)
