from app.forms import PostForm
from app.models import Post
from app import db
from flask import render_template, redirect, url_for
import datetime
from config import ALLOWED_EXTENSION, UPLOAD_IMAGES
from werkzeug import secure_filename
import os

def allowedFile(filename):
    return '.' in   filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSION

def notes(request):
    form = PostForm()
    if form.validate_on_submit():

        f = request.files['file']
        if f and allowedFile(f.filename):
            filename = secure_filename(f.filename)
            f.save(os.path.join(UPLOAD_IMAGES, filename))

        post = Post(text = form.text.data, timestamp = datetime.datetime.utcnow(), imagePath=f.filename);
        db.session.add(post)
        db.session.commit()
        return redirect(url_for('notesView'))

    if request.method == 'POST':
        if 'img' in request.files:
            img = request.files.get('img')
            text = request.form['text']
            post = Post.query.filter_by(text=text.strip()).first()
            filename = secure_filename(img.filename)
            img.save(os.path.join(UPLOAD_IMAGES, filename))
            post.imagePath = filename
            db.session.commit()

        if(request.form['type'].strip() == 'delete'):
            text = request.form['text'].strip()
            delPost = Post.query.filter_by(text=text).first()
            db.session.delete(delPost)
            db.session.commit()
        if request.form['type'].strip() == 'edit':
            oldText = request.form['oldText'].strip()
            newText = request.form['newText'].strip()
            editPost = Post.query.filter_by(text=oldText).first()
            editPost.text = newText
            db.session.commit()
        if request.form['type'].strip() == 'deleteImage':
            text = request.form['text'].strip()
            editPost = Post.query.filter_by(text=text).first()
            editPost.imagePath = ""
            db.session.commit()



    posts = Post.query.order_by(Post.timestamp.desc())
    return render_template('notes.html',
                           title = 'Notes',
                           form = form,
                           posts = posts)