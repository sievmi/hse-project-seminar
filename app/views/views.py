from app import app, db

from app.models import Habit, ResultHabit
from app.views.habits import habits
from app.views.index import index
from app.views.notes import notes
from app.views.rankNotes import rankNotes
from app.views.habitsStats import habitStats
from flask import request, redirect, url_for, render_template
import datetime

@app.route('/')
@app.route('/index')
def indexView():
    return index()

@app.route('/habits',  methods=['GET', 'POST'])
def habitsView():
    return habits(request=request)


@app.route('/notes', methods=['GET', 'POST'])
def notesView():
    return notes(request=request)

@app.route('/rankNotes', methods=['GET', 'POST'])
def rankNotesView():
    return rankNotes(request=request)

@app.route('/habitsStats', methods=['GET', 'POST'])
def habitsStatsView():
    return habitStats()

@app.route('/addResultHandler', methods=['GET', 'POST'])
def  addResultHamdler():
    date = request.form['addResultDate']
    if date == "":
        return redirect(url_for('habitsView'))
    habitID = request.form['addResultHabitID']
    result = request.form['addResultResult']

    habit = Habit.query.get(habitID)

    date = datetime.datetime.strptime(date, '%Y-%m-%d').date()
    results = ResultHabit.query.filter_by(name=habit, date=date).all()
    if len(results) == 0:
        newResult = ResultHabit(date=date, name=habit, result=result)
        db.session.add(newResult)
        db.session.commit()
    else:
        results[0].result = result
        db.session.commit()

    return redirect(url_for('habitsView'))

@app.route('/showResultHandler', methods=['GET', 'POST'])
def showResultsHandler():
    month = int(request.form['showResultMonth'])
    year = int(request.form['showResultYear'])
    date = datetime.date(year=year,month=month,day=1)

    startMonth = month
    delta = datetime.timedelta(days=1)
    cntDays = 0
    habitResults = []
    while(startMonth == date.month):
        cntDays+=1
        dayResult = ResultHabit.query.filter_by(date = date).all()
        habitResults.extend(dayResult)
        date = date + delta

    resultsCalendarList = {}
    resultsCalendarArray = {}
    for curResult in habitResults:
        if curResult.name.name in resultsCalendarList:
            resultsCalendarList[curResult.name.name].append(curResult)
        else:
            resultsCalendarList[curResult.name.name] = []
            resultsCalendarList[curResult.name.name].append(curResult)


    for curResult in resultsCalendarList:
        resultsCalendarArray[curResult] = [0] * cntDays
        for cur in resultsCalendarList[curResult]:
            resultsCalendarArray[curResult][cur.date.day - 1] = cur.result

    habits = Habit.query.all()
    activeHabits = Habit.query.filter_by(active=True).all()

    return render_template("habits.html", habits = habits, activeHabits=activeHabits,
            cntDays=cntDays, date=date, resultsCalendarArray=resultsCalendarArray)

