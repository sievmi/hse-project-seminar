from flask import render_template
from app.models import Habit
from flask import request
import datetime
from app import db

def habits(request):
    # OBRABOTKA RESULTATOV IS SUBMITA
    habits = Habit.query.all()
    activeHabits = Habit.query.filter_by(active=True).all()
    if request.method == 'POST':
        if request.form['type'].strip() == "addHabit":
            name = request.form['name'].strip()
            habits = Habit.query.filter_by(name=name).all()
            if(len(habits) == 0):
                habit = Habit(name=name, active=True, delta = 0, lastStop = datetime.date.today())
                db.session.add(habit)
                db.session.commit()
                return str(habit.id)
            else:
                habit = habits[0];
                if habit.active == True:
                    return "-1";
                else:
                    habit.active = True;
                    delta = max(0, (datetime.date.today()-habit.lastStop.date()).days - 1)
                    db.session.commit()
                    return str(habit.id)
        if request.form['type'].strip() == "deleteHabit":
            id = request.form['id']
            habit = Habit.query.get(id);
            habit.active = False;
            habit.lastStop = datetime.date.today()
            db.session.commit()
            return str(habit.id)



    return render_template("habits.html", habits = habits, activeHabits=activeHabits, cntDays=31)